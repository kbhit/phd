#include "productions.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

__global__ void executeProductionsOnGPU(P2Production * production) {
	P2Production * currentProduction = &production[blockIdx.x];
	Vertex * boundNode = currentProduction->getBoundNode();
	currentProduction->apply(boundNode);
}

void BatchProduction::executeAllInParallel() {
	int productionCount = productionsToExecute.size();
	P2Production * productionsOnGPU;
	cudaMalloc((void **) &productionsOnGPU, sizeof(P2Production *) * productionCount);
	size_t memoryToAllocate = 0;
	int i = 0;
	for (vector<Production *>::iterator cit = productionsToExecute.begin(); cit != productionsToExecute.end(); ++cit) {
		Production * productionOnDevice = (*cit)->allocateToDevice();
		cudaMemcpy(&productionsOnGPU[i++], productionOnDevice, sizeof(P2Production *), cudaMemcpyDeviceToDevice);
		memoryToAllocate += (*cit)->getSize();
	}
	
	executeProductionsOnGPU<<<productionCount, 1>>>(productionsOnGPU);
	cudaMemcpy(&productionsToExecute, productionsOnGPU, memoryToAllocate, cudaMemcpyDeviceToHost);
	cudaFree(productionsOnGPU);
}

void BatchProduction::addProduction(Production * production) {
	productionsToExecute.push_back(production);
}