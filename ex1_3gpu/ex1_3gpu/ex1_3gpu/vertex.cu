#include "commons.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
using namespace std;


__host__ __device__ size_t mat3x3::get_size() {
	return sizeof(this);
}

__host__ __device__ Vertex::Vertex(char * nodeLabel) : label(nodeLabel), 
	leftChild(NULL), rightChild(NULL), parent(NULL), nodeData(NULL) {

}

__host__ __device__ Vertex::Vertex(const Vertex &vertex) {
	label = vertex.label;
	leftChild = vertex.leftChild;
	rightChild = vertex.rightChild;
	parent = vertex.parent;
	nodeData = vertex.nodeData;
}

__host__ __device__  Vertex * Vertex::getParent() {
	return parent;
}

__host__ __device__  void Vertex::setParent(Vertex * parent) {
	Vertex::parent = parent;
}

__host__ __device__  Vertex * Vertex::getLeftChild() {
	return leftChild;
}

__host__ __device__  Vertex * Vertex::getRightChild() {
	return rightChild;
}

__host__ __device__  void Vertex::setLeftChild(Vertex * node) {
	leftChild = node;
	leftChild->setParent(this);
}

__host__ __device__  void Vertex::setRightChild(Vertex * node) {
	rightChild = node;
	rightChild->setParent(this);
}

__host__ __device__  void Vertex::setData(mat * data) {
	nodeData = data;
}

__host__ __device__  mat * Vertex::getData() {
	return nodeData;
}

__host__ __device__ size_t Vertex::getMemsize() {
	size_t totalSize = sizeof(this);
	totalSize += sizeof(label);
	if(parent) {
		totalSize += parent->getMemsize();
	}
	if(leftChild) {
		totalSize += leftChild->getMemsize();
	}
	if(rightChild) {
		totalSize += rightChild->getMemsize();
	}
	if(nodeData) {
		totalSize += nodeData->get_size();
	}
	return totalSize;
}

__host__ Vertex * Vertex::allocateToDevice() {
	Vertex * vertexOnDevice; 
	size_t vertexSize = sizeof(Vertex);
	cudaMalloc((void **) &vertexOnDevice, vertexSize);
	cudaMemcpy(vertexOnDevice, this, vertexSize, cudaMemcpyHostToDevice);
	
	char * labelOnDevice;
	cudaMalloc((void **) &labelOnDevice, sizeof(label));
	cudaMemcpy((void **) &labelOnDevice, label, sizeof(label), cudaMemcpyHostToDevice);
	cudaMemcpy(&(vertexOnDevice->label), &labelOnDevice, sizeof(char *), cudaMemcpyDeviceToDevice);

	if(leftChild) {
		Vertex * leftChildOnDevice = leftChild->allocateToDevice();
		cudaMemcpy(&(vertexOnDevice->leftChild), &leftChildOnDevice, sizeof(Vertex *), cudaMemcpyDeviceToDevice);
	}

	if(rightChild) {
		Vertex * rightChildOnDevice = rightChild->allocateToDevice();
		cudaMemcpy(&(vertexOnDevice->rightChild), &rightChildOnDevice, sizeof(Vertex *), cudaMemcpyDeviceToDevice);
	}
	
	if(parent) {
		Vertex * parentOnDevice = parent->allocateToDevice();
		cudaMemcpy(&(vertexOnDevice->parent), &parentOnDevice, sizeof(Vertex *), cudaMemcpyDeviceToDevice);
	}

	if(nodeData) {
		mat * dataOnDevice;
		cudaMalloc((void **) &dataOnDevice, nodeData->get_size());
		cudaMemcpy(&dataOnDevice, &nodeData, nodeData->get_size(), cudaMemcpyHostToDevice);
		cudaMemcpy(&(vertexOnDevice->nodeData), &dataOnDevice, sizeof(mat3x3 *), cudaMemcpyDeviceToDevice); // !!!!!!!!!!!!!!!! not necessarily mat3x3
	}

	return vertexOnDevice;	
}