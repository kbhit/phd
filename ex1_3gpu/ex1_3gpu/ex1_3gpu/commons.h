#include <iostream>
#include "device_launch_parameters.h"
using namespace std;

struct mat {
	__host__ __device__ virtual size_t get_size() = 0;
};

struct mat3x3 : mat {
	double a[3][3], b[3], x[3];
	__host__ __device__ virtual size_t get_size();
};

class Vertex {

	char * label;
	Vertex * leftChild, *rightChild, *parent;
	mat * nodeData;

public:

	__host__ __device__ Vertex(char * nodeLabel);
	__host__ __device__ Vertex(const Vertex &vertex);
	__host__ __device__ Vertex * getParent();
	__host__ __device__ Vertex * getLeftChild();
	__host__ __device__ Vertex * getRightChild();
	__host__ __device__ void setParent(Vertex *);
	__host__ __device__ void setLeftChild(Vertex *);
	__host__ __device__ void setRightChild(Vertex *);
	__host__ __device__ mat * getData();
	__host__ __device__ void setData(mat * data);
	__host__ __device__ size_t getMemsize();
	__host__ Vertex * allocateToDevice();

};