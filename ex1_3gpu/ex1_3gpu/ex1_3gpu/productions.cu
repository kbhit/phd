#include "productions.h"
#include "device_launch_parameters.h"
#include "cuda_runtime.h"

__host__ Production::Production(Vertex * root) {
	boundRoot = root;
}

__host__ Production * Production::allocateToDevice() {
	Production * productionOnDevice;
	size_t productionSize = sizeof(this);
	cudaMalloc((void **) &productionOnDevice, productionSize);
	cudaMemcpy(productionOnDevice, this, productionSize, cudaMemcpyHostToDevice);

	Vertex * boundRootOnDevice = boundRoot->allocateToDevice();
	size_t boundRootPointerSize = sizeof(Vertex *);
	cudaMemcpy(&(productionOnDevice->boundRoot), boundRootOnDevice, boundRootPointerSize, cudaMemcpyDeviceToDevice);

	return productionOnDevice;
}

/*
	Run this with CUDA
*/
__host__  __device__ void Production::execute() {
	//apply(boundRoot);
}

__host__ __device__ size_t Production::getSize() {
	return sizeof(this) + boundRoot->getMemsize();
}

__host__ __device__  Vertex * Production::getBoundNode() {
	return boundRoot;
}

__host__ __device__ Vertex * P1Production::apply(Vertex * v) {
	Vertex * leftChild = new Vertex("T");
	v->setLeftChild(leftChild);
	Vertex * rightChild = new Vertex("T");
	v->setRightChild(rightChild);
	return v;
}

__host__ P1Production::P1Production(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * P2Production::apply(Vertex * v) {
	Vertex * leftChild = new Vertex("T");
	v->setLeftChild(leftChild);
	Vertex * rightChild = new Vertex("T");
	v->setRightChild(rightChild);
	return v;
}

__host__ P2Production::P2Production(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * P3Production::apply(Vertex * v) {
	Vertex * leftChild = new Vertex("node");
	v->setLeftChild(leftChild);
	Vertex * rightChild = new Vertex("node");
	v->setRightChild(rightChild);
	return v;
}

__host__ P3Production::P3Production(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * AProduction::apply(Vertex * v) {
	mat3x3 m;
    m.a[1][1] = 1.0;
    m.a[2][1] = -1.0;
    m.a[1][2] = -1.0;
    m.a[2][2] = 1.0;
    m.b[1] = 0.0;
    m.b[2] = 0.0;
	v->setData(&m);
	return v;
}

__host__ AProduction::AProduction(Vertex * v) : Production(v) {
	
}


__host__ __device__ Vertex * A1Production::apply(Vertex * v) {
	mat3x3 m;
	m.a[1][1] = 1.0;
    m.a[2][1] = -1.0;
    m.a[1][2] = 0.0;
    m.a[2][2] = 1.0;
    m.b[1] = 0.0;
    m.b[2] = 0.0;
	v->setData(&m);
	return v;
}

__host__ A1Production::A1Production(Vertex * v) : Production(v) {
	
}


__host__ __device__ Vertex * A2Production::apply(Vertex * v) {
	mat3x3 m;
	const mat3x3 * mLeftChild = reinterpret_cast<mat3x3*>(v->getLeftChild()->getData());
	const mat3x3 * mRightChild = reinterpret_cast<mat3x3*>(v->getRightChild()->getData());

	m.a[0][0] = mLeftChild->a[2][2] + mRightChild->a[1][1];
    m.a[1][0] = mLeftChild->a[1][2];
    m.a[2][0] = mRightChild->a[2][1];
    m.a[0][1] = mLeftChild->a[2][1];
    m.a[1][1] = mLeftChild->a[1][1];
    m.a[2][1] = 0.0;
    m.a[0][2] = mRightChild->a[1][2];
    m.a[1][2] = 0.0;
    m.a[2][2] = mRightChild->a[2][2];
    m.b[0] = mLeftChild->b[2] + mRightChild->b[1];
    m.b[1] = mLeftChild->b[1];
    m.b[2] = mRightChild->b[2];
	v->setData(&m);
	return v;
}

__host__ A2Production::A2Production(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * ANProduction::apply(Vertex * v) {
	mat3x3 m;
    m.a[1][1] = 1.0;
    m.a[2][1] = -1.0;
    m.a[1][2] = -1.0;
    m.a[2][2] = 1.0;
    m.b[1] = 0.0;
    m.b[2] = 1.0 / 6.0;
	v->setData(&m);
	return v;
}

__host__ ANProduction::ANProduction(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * BSProduction::apply(Vertex * v) {
	mat3x3 * thisNode = reinterpret_cast<mat3x3*>(v->getData());
	mat3x3 * mLeftChild = reinterpret_cast<mat3x3*>(v->getLeftChild()->getData());
	mat3x3 * mRightChild = reinterpret_cast<mat3x3*>(v->getRightChild()->getData());

    mLeftChild->x[1] = thisNode->x[1];
    mLeftChild->x[2] = thisNode->x[0];
    mLeftChild->x[0] = (mLeftChild->b[0] - mLeftChild->a[0][1]
            * mLeftChild->x[1] - mLeftChild->a[0][2] * mLeftChild->x[2])
            / mLeftChild->a[0][0];
    mRightChild->x[1] = thisNode->x[0];
    mRightChild->x[2] = thisNode->x[2];
    mRightChild->x[0] = (mRightChild->b[0] - mRightChild->a[0][1]
            * mRightChild->x[1] - mRightChild->a[0][2] * mRightChild->x[2])
            / mRightChild->a[0][0];
	v->setData(thisNode);
	return v;
};

__host__ BSProduction::BSProduction(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * E2Production::apply(Vertex * v) {
	mat3x3 * thisNode = reinterpret_cast<mat3x3*>(v->getData());

    thisNode->b[0] /= thisNode->a[0][0];
    thisNode->a[0][2] /= thisNode->a[0][0];
    thisNode->a[0][1] /= thisNode->a[0][0];
    thisNode->a[0][0] /= thisNode->a[0][0];
    thisNode->b[1] -= thisNode->b[0] * thisNode->a[1][0];
    thisNode->a[1][2] -= thisNode->a[0][2] * thisNode->a[1][0];
    thisNode->a[1][1] -= thisNode->a[0][1] * thisNode->a[1][0];
    thisNode->a[1][0] -= thisNode->a[0][0] * thisNode->a[1][0];
    thisNode->b[2] -= thisNode->b[0] * thisNode->a[2][0];
    thisNode->a[2][2] -= thisNode->a[0][2] * thisNode->a[2][0];
    thisNode->a[2][1] -= thisNode->a[0][1] * thisNode->a[2][0];
    thisNode->a[2][0] -= thisNode->a[0][0] * thisNode->a[2][0];
	return v;
}

__host__ E2Production::E2Production(Vertex * v) : Production(v) {
	
}

__host__ __device__ Vertex * ERootProduction::apply(Vertex * v) {
	mat3x3 * thisNode = reinterpret_cast<mat3x3*>(v->getData());

    //divide first row by diagonal
    thisNode->b[1] /= thisNode->a[1][1];
    thisNode->a[1][2] /= thisNode->a[1][1];
    thisNode->a[1][0] /= thisNode->a[1][1];
    thisNode->a[1][1] /= thisNode->a[1][1];
    //2nd=2nd-1st*diag
    thisNode->b[0] -= thisNode->b[1] * thisNode->a[0][1];
    thisNode->a[0][2] -= thisNode->a[1][2] * thisNode->a[0][1];
    thisNode->a[0][0] -= thisNode->a[1][0] * thisNode->a[0][1];
    thisNode->a[0][1] -= thisNode->a[1][1] * thisNode->a[0][1];
    //3rd=3rd-1st*diag
    thisNode->b[2] -= thisNode->b[1] * thisNode->a[2][1];
    thisNode->a[2][2] -= thisNode->a[1][2] * thisNode->a[2][1];
    thisNode->a[2][0] -= thisNode->a[1][0] * thisNode->a[2][1];
    thisNode->a[2][1] -= thisNode->a[1][1] * thisNode->a[2][1];

    //divide second row by diagonal
    thisNode->b[0] /= thisNode->a[0][0];
    thisNode->a[0][2] /= thisNode->a[0][0];
    thisNode->a[0][0] /= thisNode->a[0][0];

    //3rd=3rd-2nd*diag
    thisNode->b[2] -= thisNode->b[0] * thisNode->a[2][0];
    thisNode->a[2][2] -= thisNode->a[0][2] * thisNode->a[2][0];
    thisNode->a[2][0] -= thisNode->a[0][0] * thisNode->a[2][0];

    //divide third row by diagonal
    thisNode->b[2] /= thisNode->a[2][2];
    thisNode->a[2][2] /= thisNode->a[2][2];

    //b.s.
    thisNode->x[2] = thisNode->b[2] / thisNode->a[2][2];
    thisNode->x[0] = (thisNode->b[0] - thisNode->a[0][2] * thisNode->x[0]) / thisNode->a[0][0];
    thisNode->x[1] = (thisNode->b[1] - thisNode->a[0][1] * thisNode->x[1] - thisNode->a[0][2] * thisNode->x[2]) / thisNode->a[1][1];
	return v;
}

__host__ ERootProduction::ERootProduction(Vertex * v) : Production(v) {
	
}