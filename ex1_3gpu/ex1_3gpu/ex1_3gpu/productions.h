#include "commons.h"
#include "device_launch_parameters.h"
#include <vector>
using namespace std;

class Production {

	Vertex * boundRoot;

public:

	Production(Vertex * root);
	__host__ Production * allocateToDevice();
	__host__ __device__ Vertex * getBoundNode();
	__host__ __device__ void execute();
	__host__ __device__ size_t getSize();

};

class BatchProduction {

	vector<Production *> productionsToExecute;

public:
	void executeAllInParallel();
	void addProduction(Production * production);
};

class P1Production : public Production {
public: 
	P1Production(Vertex * root);
//protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class P2Production : public Production {
public: P2Production(Vertex * root);
//protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class P3Production : public Production {
public: P3Production(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class AProduction : public Production {
public: AProduction(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class A1Production : public Production {
public: A1Production(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class A2Production : public Production {
public: A2Production(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class ANProduction : public Production {
public: ANProduction(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class BSProduction : public Production {
public: BSProduction(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class E2Production : public Production {
public: E2Production(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};

class ERootProduction : public Production {
public: ERootProduction(Vertex * root);
protected: 
	__host__ __device__ Vertex * apply(Vertex * v);
};