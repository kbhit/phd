#include <iostream>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "productions.h"
#include <sstream>
using namespace std;

string describe(mat3x3 mat) {
	stringstream ss;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			ss << mat.a[i][j] << " ";
		}
		ss << endl;
	}
	return ss.str();
}

mat3x3 extract_mat3x3(Production * p) {
	return *reinterpret_cast<mat3x3*>(p->getBoundNode()->getData());
}

int main() {
	//[(P1)], only one operation, don't need to run in parallel
 	Vertex * s = new Vertex("S");
	P1Production * p1 = new P1Production(s);
	p1->apply(p1->getBoundNode());

	//[(P2)1(P2)2]
	Production * p2a = new P2Production(p1->getBoundNode()->getLeftChild());
	Production * p2b = new P2Production(p1->getBoundNode()->getRightChild());
	BatchProduction * ppro1 = new BatchProduction();
	ppro1->addProduction(p2a);
	ppro1->addProduction(p2b);

	ppro1->executeAllInParallel();

	cout << "Start";
	cout << "End";
}

